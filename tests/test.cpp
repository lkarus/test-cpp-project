#include "gtest/gtest.h"
#include "arithmetic.h"

TEST(Arithmetic, Single) { 
  ASSERT_EQ(3, minus(4, 1));
  ASSERT_EQ(5, plus(4, 1));
  ASSERT_EQ(4, multiply(4,1));
  ASSERT_EQ(4, divide(4,1));
  ASSERT_EQ(0, modulo(4,1));
}

TEST(Arithmetic, Combination) { 
  ASSERT_EQ(multiply(minus(2,1),plus(2,1)), 3);
}

TEST(Comparison, LessThan) {
  ASSERT_EQ(-1, compare(0,1));
}

TEST(Comparison, Equal) {
  ASSERT_EQ(0, compare(1,1)); 
}

TEST(Comparison, LargerThan) {
  ASSERT_EQ(1, compare(2,1));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

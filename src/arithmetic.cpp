#include "arithmetic.h"

int minus(int x, int y) {
  // int ret = x - y;
  return x - y;
}

int plus(int x, int y) {
  int ret = x + y;
  return ret;
}

int multiply(int x, int y) {
  return x * y;
}

int divide(int x, int y) {
  int ret = x / y;
  return ret;
}

int modulo(int x, int y) {
  return x % y;
}

int compare(int x, int y) {
  if (x > y)
    return 1;
  else if (x < y)
    return -1;
  else
    return 0;
}
